Restaurant Recommendation System Using Relational Database [Python, MySQL, Django]	[Oct - Dec 17]
-	Implemented restaurant recommendation system based on user (eg. location, cuisine preference, rating) and restaurant (location, heterogenous review criteria) information.
-	Included features to derive review effectiveness and user trustworthiness from available data. 
